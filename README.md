# Acuparse Debian MariaDB

**MariaDB 10.5**

Multi-arch Debian Bullseye MariaDB image built for use with Acuparse.

## Changes

- Modified to run Debian Bullseye and use Debian package repositories.
- Grants required `EVENT`, `RELOAD`, and `SUPER` privileges.

See https://hub.docker.com/_/mariadb for more details.

Modified from source: https://github.com/docker-library/mariadb/tree/master
